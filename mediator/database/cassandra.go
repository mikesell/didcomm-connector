package database

import (
	"errors"
	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	"time"

	"github.com/gocql/gocql"
)

type Cassandra struct {
	session  *gocql.Session
	keyspace string
}

func NewCassandra() *Cassandra {
	session, err := newCassandraSession()
	if err != nil {
		config.Logger.Error("NewCassandra", "Error creating session:", err)
		panic("Error creating cassandra session")
	}
	return &Cassandra{
		session:  session,
		keyspace: config.CurrentConfiguration.Database.Keyspace,
	}
}

func (db *Cassandra) GetMediatorDid() (string, error) {
	iter := db.session.Query("SELECT did FROM mediator_did").Iter()
	var did string
	for iter.Scan(&did) {
		return did, nil
	}
	if err := iter.Close(); err != nil {
		return "", err
	}
	return "", nil
}

func (d *Cassandra) StoreMediatorDid(mediatorDid string) (err error) {
	t := time.Now()
	if err := d.session.Query("INSERT INTO mediator_did (did, created) VALUES (?, ?)", mediatorDid, t).Exec(); err != nil {
		return err
	}
	return nil
}

// TODO: update routingKey to endpoint
func (db *Cassandra) AddMediatee(remoteDid string, protocol string, topic string, properties map[string]string) error {
	if err := db.session.Query("INSERT INTO mediatees (remote_did, protocol, topic, properties) VALUES (?, ?, ?, ?)", remoteDid, protocol, topic, properties).Exec(); err != nil {
		return err
	}
	return nil
}

func (db *Cassandra) SetRoutingKey(remoteDid string, routingKey string) error {
	if err := db.session.Query("UPDATE mediatees SET routing_key = ? WHERE remote_did = ?", routingKey, remoteDid).Exec(); err != nil {
		return err
	}
	return nil
}

func (db *Cassandra) GetRoutingKey(remoteDid string) (string, error) {
	iter := db.session.Query("SELECT routing_key FROM mediatees WHERE remote_did = ?", remoteDid).Iter()
	var routingKey string
	for iter.Scan(&routingKey) {
		return routingKey, nil
	}
	if err := iter.Close(); err != nil {
		return "", err
	}
	return "", nil
}

func (db *Cassandra) IsMediated(remoteDid string) (bool, error) {
	iter := db.session.Query("SELECT remote_did FROM mediatees WHERE remote_did = ?", remoteDid).Iter()
	var mediatee Mediatee
	for iter.Scan(&mediatee.RemoteDid) {
		if mediatee.RemoteDid == remoteDid {
			return true, nil
		}
	}
	if err := iter.Close(); err != nil {
		return false, err
	}
	return false, nil
}

func (db *Cassandra) IsRecipientDidRegistered(recipientDid string) (bool, error) {
	iter := db.session.Query("SELECT recipient_did FROM recipient_dids WHERE recipient_did = ?", recipientDid).Iter()
	var did string
	for iter.Scan(&did) {
		if did == recipientDid {
			return true, nil
		}
	}
	if err := iter.Close(); err != nil {
		return false, err
	}
	return false, nil
}

func (db *Cassandra) RecipientAndRemoteDidBelongTogether(recipientDid string, remoteDid string) (bool, error) {
	iter := db.session.Query("SELECT remote_did FROM recipient_dids WHERE recipient_did = ?", recipientDid).Iter()
	var did string
	for iter.Scan(&did) {
		if did == remoteDid {
			return true, nil
		}
	}
	if err := iter.Close(); err != nil {
		return false, err
	}
	return false, nil
}

func (db *Cassandra) GetRecipientDids(remoteDid string) (recipientDids []string, err error) {
	iter := db.session.Query("SELECT recipient_did FROM recipient_dids WHERE remote_did = ?", remoteDid).Iter()
	var recipientDid string
	recipientDids = nil
	for iter.Scan(&recipientDid) {
		recipientDids = append(recipientDids, recipientDid)
	}
	if err := iter.Close(); err != nil {
		return nil, err
	}
	return recipientDids, nil
}

func (db *Cassandra) AddRecipientDid(remoteDid string, recipientDid string) error {
	if err := db.session.Query("INSERT INTO recipient_dids (recipient_did, remote_did) VALUES (?, ?)", recipientDid, remoteDid).Exec(); err != nil {
		return err
	}
	return nil
}

func (db *Cassandra) DeleteRecipientDid(remoteDid string, recipientDid string) error {
	// the recipient did should be unique, so we can delete it directly
	if err := db.session.Query("DELETE FROM recipient_dids WHERE recipient_did = ?", recipientDid).Exec(); err != nil {
		return err
	}
	return nil
}

func (db *Cassandra) RemoteDidBelongsToMessage(remoteDid string, messageId string) (bool, error) {
	iter := db.session.Query("SELECT recipient_did FROM messages WHERE message_id = ?", messageId).Iter()
	var dids []string
	var did string
	for iter.Scan(&did) {
		dids = append(dids, did)
	}
	if err := iter.Close(); err != nil {
		return false, err
	}
	if len(dids) == 0 {
		return false, nil
	}
	if len(dids) > 1 {
		return false, errors.New("message id is not unique")
	}
	iter = db.session.Query("SELECT remote_did FROM recipient_dids WHERE recipient_did = ?", did).Iter()
	var remoteDids []string
	var remote string
	for iter.Scan(&remote) {
		remoteDids = append(remoteDids, remote)
	}
	if err := iter.Close(); err != nil {
		return false, err
	}
	if len(remoteDids) != 1 {
		return false, errors.New("recipient did is not unique")
	}
	if remote == remoteDid {
		return true, nil
	} else {
		return false, nil
	}
}

func (m *Cassandra) GetMessageCountForRecipient(recipientDid string) (count int, err error) {
	iter := m.session.Query("SELECT COUNT(*) FROM messages WHERE recipient_did = ?", recipientDid).Iter()
	var c int
	for iter.Scan(&c) {
		count = c
	}
	if err := iter.Close(); err != nil {
		return 0, err
	}
	return count, nil
}

func (m *Cassandra) GetMessagesForRecipient(recipientDid string, limit int) ([]didcomm.Attachment, error) {
	iter := m.session.Query("SELECT message_id, recipient_did, description, filename, media_type, format, lastmod_time, byte_count, attachment_data FROM messages WHERE recipient_did = ? LIMIT ?", recipientDid, limit).Iter()
	var data didcomm.AttachmentDataBase64
	var message didcomm.Attachment
	var messages []didcomm.Attachment
	var did string
	for iter.Scan(&message.Id, &did, &message.Description, &message.Filename, &message.MediaType, &message.Format, &message.LastmodTime, &message.ByteCount, &data.Value.Base64) {
		message.Data = data
		messages = append(messages, message)
	}
	if err := iter.Close(); err != nil {
		return nil, err
	}
	return messages, nil
}

func (m *Cassandra) DeleteMessagesByIds(messageIds []string) (deletedCount int, err error) {
	// the message id should be unique, so we can delete it directly
	var count int = 0
	for _, id := range messageIds {
		if err := m.session.Query("DELETE FROM messages WHERE message_id = ?", id).Exec(); err != nil {
			return count, err
		}
		count++
	}
	return count, nil
}

func (m *Cassandra) AddMessage(recipientDid string, message didcomm.Attachment) error {
	// check if message id is unique
	iter := m.session.Query("SELECT message_id FROM messages WHERE message_id = ?", message.Id).Iter()
	var id string
	for iter.Scan(&id) {
		return errors.New("message id is not unique")
	}
	if err := m.session.Query("INSERT INTO messages (message_id, recipient_did, description, filename, media_type, format, lastmod_time, byte_count, attachment_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", message.Id, recipientDid, message.Description, message.Filename, message.MediaType, message.Format, message.LastmodTime, message.ByteCount, message.Data.(didcomm.AttachmentDataBase64).Value.Base64).Exec(); err != nil {
		return err
	}
	return nil
}

func newCassandraSession() (*gocql.Session, error) {

	dbConfig := config.CurrentConfiguration.Database

	cluster := gocql.NewCluster(dbConfig.Host)
	cluster.Port = dbConfig.Port
	cluster.Keyspace = dbConfig.Keyspace

	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: dbConfig.User,
		Password: dbConfig.Password,
	}

	session, err := cluster.CreateSession()
	if err != nil {
		config.Logger.Error("Cassandra", "Error creating session:", err)
		return nil, err
	}
	return session, nil
}
