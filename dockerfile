FROM golang:1.21

# Copies your source code into the go directory
COPY . /go/src/didcommconnector

# set work directory
WORKDIR /go/src/didcommconnector

#RUN go build to build the application with the name app
RUN go build -o app .
 
EXPOSE 9090:9090
 
CMD [ "/go/src/didcommconnector/app" ]