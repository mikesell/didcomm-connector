package main

import (
	"fmt"
	"gaiax/didcommconnector/internal/config"
	"gaiax/didcommconnector/mediator"
	"log/slog"
)

//	@title DIDComm Connector API
//	@version 1.0
//	@description The DIDCommConnector can be used as a Mediator and Connection Management Service by parties who want to set up trust with another party. The DIDCommConnector uses DIDComm v2 and provides a message layer and a management component for the following two use cases: - Pairing a cloud solution with a smartphone / app solution - DIDComm v2 based message protocols
//	@description  - Pairing a cloud solution with a smartphone / app solution - DIDComm v2 based message protocols
//	@description  - DIDComm v2 based message protocols
//	@termsOfService	http://swagger.io/terms/

//	@contact.name API Support
//	@contact.url http://www.change.this/url
//	@contact.email email@todo.io

//	@license.name Apache 2.0
//	@license.url http://www.apache.org/licenses/LICENSE-2.0.html

//	@host localhost:9090
//	@BasePath /

//	@securityDefinitions.basic BasicAuth

//	@securityDefinitions.apikey	ApiKeyAuth
//	@in header
//	@name Authorization
//	@description Description for what is this security definition being used

//	@securitydefinitions.oauth2.application	OAuth2Application
//	@tokenUrl https://example.com/oauth/token
//	@scope.write Grants write access
//	@scope.admin Grants read and write access to administrative information

//	@securitydefinitions.oauth2.implicit OAuth2Implicit
//	@authorizationUrl https://example.com/oauth/authorize
//	@scope.write Grants write access
//	@scope.admin Grants read and write access to administrative information

//	@securitydefinitions.oauth2.password OAuth2Password
//	@tokenUrl https://example.com/oauth/token
//	@scope.read Grants read access
//	@scope.write Grants write access
//	@scope.admin Grants read and write access to administrative information

// @securitydefinitions.oauth2.accessCode OAuth2AccessCode
// @tokenUrl https://example.com/oauth/token
// @authorizationUrl https://example.com/oauth/authorize
// @scope.admin Grants read and write access to administrative information

type application struct {
	logger   *slog.Logger
	mediator *mediator.Mediator
}

func main() {

	err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	if config.IsForwardTypeNats() || config.IsForwardTypeHybrid() {
		// subscribe to nats
		go mediator.ReceiveMessage()
	}

	app := application{
		logger:   config.Logger,
		mediator: mediator.NewMediator(config.Logger),
	}

	router := app.NewRouter()
	err = router.Run(":" + fmt.Sprint(config.CurrentConfiguration.Port))
	if err != nil {
		panic(err)
	}
}
