package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	intErr "gaiax/didcommconnector/internal/errors"
	"gaiax/didcommconnector/protocol"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

// PingExample godoc
// @Summary TODO
// @Schemes TODO
// @Description TODO
// @Tags Message
// @Accept json
// @Produce json
// @Success 200 {string} test
// @Router /message  [patch]
func (app *application) ReceiveMessage(context *gin.Context) {
	// TODO: replace with middleware
	mediator := app.mediator
	body, err := io.ReadAll(context.Request.Body)
	if err != nil {
		context.String(500, "Error reading request body")
		return
	}
	bodyString := string(body)
	msg, err := mediator.UnpackMessage(bodyString)
	// TODO: check created_time and expiration_time is valid
	if err != nil {
		config.Logger.Error("ReceiveMessage", "Error unpacking message:", err)
		context.String(http.StatusInternalServerError, "Error unpacking message")
		return
	}

	// handle message
	var responseMsg didcomm.Message = didcomm.Message{}
	if strings.HasPrefix(msg.Type, protocol.PIURI_COORDINATE_MEDIATION) {

		coordinateMediation := protocol.NewCoordinateMediation(mediator)
		responseMsg, err = coordinateMediation.Handle(msg)
	} else if strings.HasPrefix(msg.Type, protocol.PIURI_TRUST_PING) {
		trustping := protocol.NewTrustPing(mediator)
		responseMsg, err = trustping.Handle(msg)
		if err != nil {
			switch {
			case errors.Is(err, intErr.ErrNoPingResponseRequested):
				context.String(http.StatusOK, "")
			default:
				context.String(http.StatusInternalServerError, "Error handling trust ping")
			}
			return
		}
	} else if strings.HasPrefix(msg.Type, protocol.PIURI_ROUTING) {
		routing := protocol.NewRouting(mediator)
		responseMsg, err = routing.Handle(msg)
		if responseMsg.Type == "" {
			context.String(http.StatusOK, "")
			return
		}

	} else if strings.HasPrefix(msg.Type, protocol.PIURI_MESSAGEPICKUP) {
		messagePickup := protocol.NewMessagePickup(mediator)
		responseMsg, err = messagePickup.Handle(msg)

	} else {
		config.Logger.Warn("Message type not handled yet.")
		responseMsg = protocol.PR_UNKNOWN_MESSAGE_TYPE
	}

	if err != nil {
		config.Logger.Error("Error while handling a didcomm request", "msg", err)
	}

	responseMsg.To = &[]string{*msg.From}
	responseMsg.From = &app.mediator.Did
	t := uint64(time.Now().UTC().Unix())
	responseMsg.CreatedTime = &t

	var packMsg string
	if config.CurrentConfiguration.DidComm.IsMessageEncrypted {
		packMsg, err = mediator.PackEncryptedMessage(responseMsg, *msg.From, mediator.Did)
	} else {
		packMsg, err = mediator.PackPlainMessage(responseMsg)
	}

	if err != nil {
		context.String(http.StatusInternalServerError, "Error packing message")
		return
	}

	var jsonMap map[string]interface{}
	err = json.Unmarshal([]byte(packMsg), &jsonMap)
	if err != nil {
		context.String(http.StatusInternalServerError, "Error marshling message")
		return
	}

	context.JSON(http.StatusOK, jsonMap)
}

func (app *application) InvitationMessage(context *gin.Context) {
	mediator := app.mediator
	oob := protocol.NewOutOfBand(mediator)

	msg, err := oob.Handle()
	if err != nil {
		context.String(http.StatusInternalServerError, "Error handling out of band")
		return
	} else {
		msg64 := base64.StdEncoding.EncodeToString([]byte(msg))
		oob := fmt.Sprintf("%s?_oob=%s", config.CurrentConfiguration.Url, msg64)
		context.String(http.StatusOK, oob)
	}
}
