package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type SynchronizationController struct{}

type Message struct {
	Username string `json:"username"`
	Text     string `json:"text"`
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		// Allow all connections
		return true
	},
}

var clients = make(map[*websocket.Conn]bool)

// can be tested with: wscat -c ws://localhost:9090/synchronization -x '{"username":"name", "text":"text"}'

func (sc SynchronizationController) Synchronization(context *gin.Context) {

	fmt.Println("websocket called!")

	conn, err := upgrader.Upgrade(context.Writer, context.Request, nil)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	clients[conn] = true

	go handleWebSocketConnection(conn)
}

func handleWebSocketConnection(conn *websocket.Conn) {
	for {
		var message Message
		err := conn.ReadJSON(&message)
		if err != nil {
			conn.Close()
			delete(clients, conn)
			break
		}
		fmt.Println(message)
	}
}
