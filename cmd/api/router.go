package main

import (
	_ "gaiax/didcommconnector/docs"
	"gaiax/didcommconnector/internal/config"

	"github.com/gin-gonic/gin"
	sloggin "github.com/samber/slog-gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (app *application) NewRouter() *gin.Engine {

	if config.IsProd() {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.New()
	router.Use(sloggin.New(config.Logger))
	// connection
	connectionsGroup := router.Group("connections")
	connectionsGroup.POST("", app.CreateConnection)
	connectionsGroup.PATCH(":did", app.UpdateConnection)
	connectionsGroup.DELETE(":did", app.DeleteConnection)
	connectionsGroup.GET(":did", app.ConnectionInformation)
	connectionsGroup.GET("", app.ListConnections)
	connectionsGroup.GET("status/:did", app.ConnectionStatus)
	connectionsGroup.POST("block/:did", app.BlockConnection)

	// messages
	messagesGroup := router.Group("message")
	messagesGroup.POST("receive", app.ReceiveMessage)
	messagesGroup.GET("invitation", app.InvitationMessage)

	// synchronization
	sc := new(SynchronizationController)
	syncGroup := router.Group("synchronization")
	syncGroup.GET("", sc.Synchronization)

	// swagger
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
