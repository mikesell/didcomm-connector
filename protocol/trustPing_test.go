package protocol_test

import (
	"encoding/json"
	"errors"
	"gaiax/didcommconnector/didcomm"
	intErr "gaiax/didcommconnector/internal/errors"
	"gaiax/didcommconnector/mediator"
	"gaiax/didcommconnector/protocol"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
)

type body struct {
	ResponseRequested bool `json:"response_requested"`
}

var med = mediator.NewMediator(slog.Default())
var tp = protocol.NewTrustPing(med)

func TestHandle_JsonSyntaxError(t *testing.T) {

	// Sample data
	msg := didcomm.Message{}

	response, err := tp.Handle(msg)

	// Check if the result matches the expected outcome
	expectedResult := didcomm.Message{}
	var expectedErr *json.SyntaxError

	assert.Equal(t, expectedResult, response)
	assert.True(t, errors.As(err, &expectedErr))
}

func TestHandle_ResponseRequesteFalse(t *testing.T) {

	// Sample data
	messageBody := body{
		ResponseRequested: false,
	}

	jsonBody, _ := json.Marshal(messageBody)

	msg := didcomm.Message{
		Id:   "123",
		Type: protocol.PIURI_TRUST_PING,
		Body: string(jsonBody),
	}

	response, err := tp.Handle(msg)

	// Check if the result matches the expected outcome
	expectedResult := didcomm.Message{}
	expectedError := intErr.ErrNoPingResponseRequested

	assert.Equal(t, expectedResult, response)
	assert.Equal(t, expectedError, err)
}

func TestHandle_ResponseRequesteTrue(t *testing.T) {

	// Sample data
	messageBody := body{
		ResponseRequested: true,
	}

	jsonBody, _ := json.Marshal(messageBody)

	msg := didcomm.Message{
		Id:   "123",
		Type: protocol.PIURI_TRUST_PING,
		Body: string(jsonBody),
	}

	response, err := tp.Handle(msg)

	// Check if the result matches the expected outcome
	assert.Equal(t, protocol.PIURI_TRUST_PING_RESPONSE, response.Type)
	assert.Equal(t, msg.Id, *response.Thid)
	assert.Equal(t, nil, err)
}
