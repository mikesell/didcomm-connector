package protocol

import (
	"bytes"
	b64 "encoding/base64"
	"errors"
	"gaiax/didcommconnector/didcomm"
	"gaiax/didcommconnector/internal/config"
	intErr "gaiax/didcommconnector/internal/errors"
	"gaiax/didcommconnector/mediator"
	"net/http"
	"time"
)

// https://identity.foundation/didcomm-messaging/spec/#routing-protocol-20

const PIURI_ROUTING = "https://didcomm.org/routing/2.0/"
const PIURI_ROUTING_FORWARD = "https://didcomm.org/routing/2.0/forward"

type Routing struct {
	mediator *mediator.Mediator
}

func NewRouting(mediator *mediator.Mediator) *Routing {
	return &Routing{
		mediator: mediator,
	}
}

func (rt *Routing) Handle(message didcomm.Message) (response didcomm.Message, err error) {

	switch message.Type {
	case PIURI_ROUTING_FORWARD:
		response, err = rt.handleForward(message)
	default:
		err = intErr.ErrUnknownMessageType
		response = PR_INTERNAL_SERVER_ERROR
	}
	return
}

func (rt *Routing) handleForward(message didcomm.Message) (pr ProblemReport, err error) {
	t := uint64(time.Now().UTC().Unix())
	if message.ExpiresTime != nil && *message.ExpiresTime < t {
		return PR_EXPIRED_MESSAGE, errors.New("message has expired")
	}
	type requestBody struct {
		RecipientDid string `json:"next"`
	}
	var body requestBody
	body, err = extractBody[requestBody](message)

	if err != nil {
		return PR_COULD_NOT_FORWARD_MESSAGE, err
	}
	isRegistered, err := rt.mediator.Database.IsRecipientDidRegistered(body.RecipientDid)
	if err != nil {
		return PR_COULD_NOT_FORWARD_MESSAGE, err
	}
	if len(*message.Attachments) != 1 {
		return PR_COULD_NOT_FORWARD_MESSAGE, errors.New("message must have exactly one attachment")
	}
	attachement := (*message.Attachments)[0]

	if isRegistered {
		err = rt.mediator.Database.AddMessage(body.RecipientDid, attachement)
		if err != nil {
			return PR_COULD_NOT_FORWARD_MESSAGE, err
		}
		// send message to cpcm (cloud)
		err := mediator.SendMessage(attachement)
		if err != nil {
			config.Logger.Error("Unable to send message to cloud", "err", err)
			return PR_COULD_NOT_FORWARD_MESSAGE, err
		}

	} else {
		isMediated, err := rt.mediator.Database.IsMediated(*message.From)
		if err != nil {
			return PR_COULD_NOT_FORWARD_MESSAGE, err
		}
		if !isMediated {
			return PR_NOT_MEDIATED, errors.New("message can only be forwarded when the sender is a mediatee")
		}

		didDoc, err := rt.mediator.DidResolver.ResolveDid(body.RecipientDid)
		if err != nil {
			return PR_COULD_NOT_FORWARD_MESSAGE, err
		}
		if len(didDoc.Service) != 1 {
			return PR_COULD_NOT_FORWARD_MESSAGE, errors.New("didDoc must have exactly one service")
		}

		service := didDoc.Service[0]
		serviceEndpoint := service.ServiceEndpoint.(didcomm.ServiceKindDidCommMessaging)
		pr, err := rt.ForwardMessage(attachement, body.RecipientDid, serviceEndpoint.Value.Uri)
		if err != nil {
			return pr, err
		}
	}

	return didcomm.Message{}, nil
}

func (rt *Routing) ForwardMessage(message didcomm.Attachment, recipientDid string, endpoint string) (pr ProblemReport, err error) {
	messageDecoded := message.Data.(didcomm.AttachmentDataBase64).Value.Base64
	body, err := b64.StdEncoding.DecodeString(messageDecoded)
	if err != nil {
		return PR_COULD_NOT_FORWARD_MESSAGE, err
	}
	r := bytes.NewReader(body)
	resp, err := http.Post(endpoint, "application/didcomm-plain+json", r)
	if err != nil {
		return PR_COULD_NOT_FORWARD_MESSAGE, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return PR_NEXT_DENIED_MESSAGE, errors.New("Forwarded message was not accepted by the next recipient")
	}
	return didcomm.Message{}, nil
}
