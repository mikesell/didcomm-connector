rust_uniffi = didcomm-rust/uniffi/
rust_release = $(rust_uniffi)/target/release/
didcomm_rust_lib = didcomm/lib

all: build test

build: build-rust build-swagger build-go

install: install-air install-swagger

clean: git-clean

build-swagger:
	go get github.com/swaggo/swag && swag init --parseInternal

build-rust:
	git submodule init && git submodule update 
	cd $(rust_uniffi) && cargo build --release
	# create lib dirctory if it does not exist
	mkdir -p $(didcomm_rust_lib)
	cp $(rust_release)libdidcomm_uniffi.* didcomm/lib
	# this pulls out ELF symbols, 80% size reduction!
	# TODO: strip didcomm/libdidcomm.*

build-go:
	go get ./cmd/api/
	go build ./cmd/api/

install-swagger:
	go install github.com/swaggo/swag/cmd/swag@latest

install-air:
	go install github.com/cosmtrek/air@latest

dev:
	export LD_LIBRARY_PATH=${PWD}/didcomm/lib && air

test:
	export LD_LIBRARY_PATH=${PWD}/didcomm/lib && go test ./...

git-clean:
	git clean -dfX

docker-build:
	cd deployment/docker && docker build . --tag didcommconnector --build-context files=../..

docker-run:
	docker run -d --name dcc didcommconnector

docker-start:
	docker start dcc

docker-clean:
	docker rm -f dcc
	docker image rm didcommconnector:latest

# install this first: https://github.com/golang-migrate/migrate
migrate-force-1: 
	migrate -database cassandra://localhost:9042/dcc -path database/migrations/ force 1
	migrate -database cassandra://localhost:9042/dcc?x-multi-statement=true -path database/migrations/ up